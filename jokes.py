from nicegui import ui
import requests
import json 

class Data:
    def __init__(self):
        self.text = " "

if __name__ in {"__main__", "__mp_main__"}:
    data = Data()
    def getJoke():
        data.text = json.loads(requests.get("https://api.chucknorris.io/jokes/random").text)["value"]
    def clearJoke():
        data.text = ""

    ui.html("<strong>Putting some things together</strong>")
    ui.label().bind_text_from(data, "text")
    ui.button('Tell me a Joke!',  on_click=getJoke)
    ui.button('Clear Output',  on_click=clearJoke)

    ui.run()