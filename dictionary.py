from nicegui import ui
import requests
import json 

class Data:
    def __init__(self):
        self.input = " "
        self.output = ""
    def setInput(self, value):
        self.input = value

if __name__ in {"__main__", "__mp_main__"}:
    data = Data()
    def getDefinition():
        pureText  = requests.get("https://api.dictionaryapi.dev/api/v2/entries/en/" + theInput.value).text

        data = json.loads(pureText)
        output = "<p><strong>Word:</strong> " + str(data[0]["word"]) + "</p>"
       
        for meaning in data[0]["meanings"]:
            output = output + "<p><strong>Part of Speech: </strong>" + str(meaning["partOfSpeech"]) + "</p><br />"
            for definition in meaning["definitions"]:
                output = output + "<p>" + str(definition["definition"]) + "</p>"
            output = output + "<hr /><br />"
        display.set_content(output)

    ui.html("<strong>A Web Interface for dictionaryapi.dev</strong>")
    ui.label().bind_text_from(data, "input")
    theInput = ui.input("Enter an English word:")
    ui.button('Search the Dictionary',  on_click=getDefinition)
    display = ui.html()

    ui.run()